import * as React from 'react';


import {createDrawerNavigator, DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import {Text} from 'react-native';
import ToiLaAi from './pages/ToiLaAi';
import Profile from './pages/Profile';
import Contact from './pages/Contact';
import {NavigationContainer} from '@react-navigation/native';
import LeftNav from './pages/LeftMenu/LeftNav';

function CustomDrawerContent(props) {


    let focus0 = false;
    let focus1 = false;
    let focus2 = false;
    if(myState!=null){
        let myActive = myState.index;
        if(myActive==0){
            focus0 = true;
        }
        if(myActive==1){
            focus1 = true;
        }
        if(myActive==2){
            focus2 = true;
        }
    }

    return (
        <DrawerContentScrollView {...props}>
            {/*<DrawerItemList {...props} />*/}
            <Text>Có chàng trai rất thơ ngây</Text>
            <DrawerItem
                label="Tôi là ai?" focused={focus0}
                onPress={() =>{ myState.index = 0; props.navigation.navigate('Contact')}}
            />
            <DrawerItem
                label="Kỹ năng tôi có gì?" focused={focus1}
                onPress={() =>{ myState.index = 1; props.navigation.navigate('Profile')}}
            />
            <DrawerItem  focused={focus2}
                         label="Liên hệ"
                         onPress={() => {myState.index = 2;props.navigation.navigate('Contact')}}
            />

        </DrawerContentScrollView>
    );
}

const Drawer = createDrawerNavigator();
function MyDrawer() {

    return (
        <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
            <Drawer.Screen name="ToiLaAi" component={ToiLaAi} />
            <Drawer.Screen name="Profile" component={Profile} />
            <Drawer.Screen name="Contact" component={Contact} />

        </Drawer.Navigator>
    );
}
let myState={index:0};
export default function AppTe() {

    return (

        <LeftNav/>
    );
}
