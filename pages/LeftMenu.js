import * as React from 'react';
import { View,Button } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Screen1 from "./Screen1";
import Screen2 from "./Screen2";
import { createStackNavigator } from '@react-navigation/stack';
import Screen3 from './Screen3';



const Drawer = createDrawerNavigator();

export default function LeftMenu() {
    return (
        <NavigationContainer>
            <Drawer.Navigator initialRouteName="Screen3" drawerContent={props => <Screen1 {...props} />}>
                <Drawer.Screen name="Screen1" component={Screen1} />
                <Drawer.Screen name="Screen2" component={Screen2} />
                <Drawer.Screen name="Screen3" component={Screen3} />

            </Drawer.Navigator>



        </NavigationContainer>
    );
}
