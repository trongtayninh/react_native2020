//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {StyleSheet, View, Text, Button} from 'react-native';
import {Header} from 'react-native-elements';


import Screen1 from './Screen1';
import Screen2 from './Screen2';
import Screen3 from './Screen3';

import LeftCom from './HeaderCom/LeftCom';
import RightCom from './HeaderCom/RightCom';
import HeaderCom from './HeaderCom/HeaderCom';
// import all basic components

export default class Contact extends Component {



    //Screen1 Component
    render() {

        return (
            <View>

                <HeaderCom navigation={this.props.navigation} text={"Liên hệ"} />

                <View style={styles.MainContainer}>

                    <Text style={{ fontSize: 23 }}>Liên hệ : Zalo,facebook:0989758864 </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        // flex: 1,
        padding: 20,
        alignItems: 'center',
        // marginTop: 50,
        justifyContent: 'center',
    },
});
