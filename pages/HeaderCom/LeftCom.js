//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {StyleSheet, View, Text, Button} from 'react-native';
import {Icon} from 'react-native-elements/src/index';

// import all basic components

export default class LeftCom extends Component {
    OpenMenu=()=>{
        this.props.navigation.openDrawer();
    }
    //Screen1 Component
    render() {

        return (
            <View style={styles.MainContainer}>
               <Icon name={"menu"} onPress={()=>{this.OpenMenu()}} />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {

    },
});
