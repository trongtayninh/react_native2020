//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import {StyleSheet, View, Text, Button} from 'react-native';
import {Header} from 'react-native-elements';
import LeftCom from './LeftCom';
import RightCom from './RightCom';


export default class HeaderCom extends Component {



    //Screen1 Component
    render() {

        return (

                <Header  containerStyle={{height: 50, paddingTop:0}}
                    placement="left"
                    leftComponent={<LeftCom navigation={this.props.navigation}/>}
                    centerComponent={{ text: this.props.text, style: { color: '#fff' } }}
                    rightComponent={<RightCom navigation={this.props.navigation}/>}
                />
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        // flex: 1,
        // paddingTop: 20,
        alignItems: 'center',
        // marginTop: 50,
        justifyContent: 'center',
    },
});
