import * as React from 'react';
import { View, Text, Button } from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItem,
} from '@react-navigation/drawer';

import Contact from '../Contact';


export default function ContentMenu(props,myState) {

    console.log(myState);
    let focus0 = false;
    let focus1 = false;
    let focus2 = false;
    if(myState!=null){
        let myActive = myState.index;
        if(myActive==0){
            focus0 = true;
        }
        if(myActive==1){
            focus1 = true;
        }
        if(myActive==2){
            focus2 = true;
        }
    }

    return (
        <DrawerContentScrollView {...props}>
            {/*<DrawerItemList {...props} />*/}
            <Text>Có chàng trai rất thơ ngây1</Text>
            <DrawerItem
                label="Tôi là ai?" focused={focus0}
                onPress={() =>{ myState.index = 0; props.navigation.navigate('ToiLaAi')}}
            />
            <DrawerItem
                label="Kỹ năng tôi có gì?" focused={focus1}
                onPress={() =>{ myState.index = 1; props.navigation.navigate('Profile')}}
            />
            <DrawerItem  focused={focus2}
                         label="Liên hệ"
                         onPress={() => {myState.index = 2;props.navigation.navigate('Contact')}}
            />

        </DrawerContentScrollView>
    );
}

