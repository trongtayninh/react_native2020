import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';
import ToiLaAi from '../ToiLaAi';
import Profile from '../Profile';
import Contact from '../Contact';
import ContentMenu from './ContentMenu';


function CustomDrawerContent(props) {


    let focus0 = false;
    let focus1 = false;
    let focus2 = false;
    if(myState!=null){
        let myActive = myState.index;
        if(myActive==0){
            focus0 = true;
        }
        if(myActive==1){
            focus1 = true;
        }
        if(myActive==2){
            focus2 = true;
        }
    }

    return (
        <DrawerContentScrollView {...props}>
            {/*<DrawerItemList {...props} />*/}
            <Text>Có chàng trai rất thơ ngâyxx</Text>
            <DrawerItem
                label="Tôi là ai?" focused={focus0}
                onPress={() =>{ myState.index = 0;console.log(props)}}
            />
            <DrawerItem
                label="Kỹ năng tôi có gì?" focused={focus1}
                onPress={() =>{ myState.index = 1; props.navigation.navigate('Profile')}}
            />
            <DrawerItem  focused={focus2}
                         label="Liên hệ"
                         onPress={() => {myState.index = 2;props.navigation.navigate('Contact')}}
            />

        </DrawerContentScrollView>
    );
}

const Drawer = createDrawerNavigator();
function MyDrawer() {

    return (
        <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
            <Drawer.Screen name="ToiLaAi" component={ToiLaAi} />
            <Drawer.Screen name="Profile" component={Profile} />
            <Drawer.Screen name="Contact" component={Contact} />

        </Drawer.Navigator>
    );
}
let myState={index:0};
export default function LeftNav() {

    return (
        <NavigationContainer onStateChange={state => {  }}>
            <MyDrawer />
        </NavigationContainer>
    );
}
